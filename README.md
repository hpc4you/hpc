# hpc4you toolkit

**hpc4you toolkit**(以下简称工具套件), 协助您通过`Copy+Paste+Enter`方式,

- 给工作站安装 slurm 调度器,
- 或者搭建 slurm 调度的并行计算集群.

工具套件有偿提供.

搭建集群, 如此简单. 仅需 Copy+Paste+Enter.
![操作演示](hpc4you-build-HPC-intro.gif)

## Quick Start

1. 移除服务器上所有 U 盘、移动硬盘等外接设备, 运行

```
bash <(curl https://gitee.com/hpc4you/hpc/raw/master/getInfo.sh)
```

或者运行指令

```
curl http://tophpc.top:1080/getInfo.sh | bash
```

按提示电邮联系`ask@hpc4you.top`. 如有疑问, 请看视频教程[BV1NY4y1C7ya](https://b23.tv/B2wG2qO)🔗

2. 收集机器 IP, 录入到/etc/hosts 文件 视频教程[bv19A4y1U7uX](https://www.bilibili.com/video/bv19A4y1U7uX)🔗

3. 上传工具包 视频教程[BV1FN4y1T7r7](https://www.bilibili.com/video/BV1FN4y1T7r7)🔗

4. 解压工具包, 运行指令

```
source code
```

根据屏幕提示复制粘贴按回车; 集群就绪.

## 视频演示

1. 使用 _**v2 集群版**_ 搭建集群简要介绍, 请看 B 站视频, [BV1GY411w7ZV](https://www.bilibili.com/video/BV1GY411w7ZV)🔗 以微软用户视角, 有旁白讲解.
2. 使用 _**v3.1 集群版**_ 搭建具有 Web 前端的集群, 请看 B 站视频合集, [视频合集点我访问](https://space.bilibili.com/470332016/channel/collectiondetail?sid=891314)🔗.
3. 使用 _**v2 单机版**_ 给单机工作站无痛安装 SLURM 调度器, 演示操作在此, [BV1Gg411R7tt](https://www.bilibili.com/video/BV1Gg411R7tt)🔗.

请留意,

- **v2**支持 RHEL7, RHEL8, RHEL9 及其兼容系统; 支持 Ubuntu 20.04 和 22.04 及其兼容系统; 华为 OpenEuler 22.03 LTS.
- **v3.1**, 支持 RHEL7, RHEL8, RHEL9 及其兼容系统; 支持 Ubuntu 20.04 和 22.04 及其兼容系统. 
- **v2 和 v3.1**, 使用流程雷同, 仅是模块名称有差异.

操作流程概括为: 收集机器信息 --> 获取工具 --> 上传解压 --> 输入`source code`指令 --> 集群就绪.

如有闲工夫, [请点击继续阅读](https://gitee.com/hpc4you/hpc/blob/master/more_info.md).
For the English version, please [click here](https://hpc4you.github.io)🔗.

## 版本及优惠

1. v2 单机, 单机/工作站/服务器傻瓜式自动部署 slurm 调度器, 243 元起
1. v3.1 单机, 单机/工作站/服务器傻瓜式自动部署 slurm 调度器和 Web 操作界面, 533 元起
1. v2 集群, SSH 命令行界面科学计算集群, 393 元起
1. v3.1 集群, 具有 Web 界面的科学计算集群, 995 元起
1. 更多版本信息, [请点击查阅](https://gitee.com/hpc4you/hpc/blob/master/version_info.md)
1. 更多优惠信息, [请点击查阅](https://gitee.com/hpc4you/hpc/blob/master/price_info.md)

## 在线体验

目前, HPC via Web, 提供在线体验.
这是采用**v3.1 集群版**处理完毕后, 集群该有的使用方式:

- 具有 Web 界面, 可以在浏览器中完成所有操作, 包括但不限于在浏览器中启用 Linux 桌面
- 用户管理, 资源分配均在 Web 界面进行.
- 同时支持传统的 SSH 命令行界面.
- [点击获取测试信息](https://gitee.com/hpc4you/hpc/blob/master/v3.1-demo.md)

## 操作演示

1. B 站视频, https://www.bilibili.com/video/bv19A4y1U7uX 该视频有旁白解读, 推荐观看.
2. B 站视频, https://www.bilibili.com/video/BV11S4y1h7PR 以微软用户视角, 演示集群搭建, 有旁白讲解.
3. B 站, 找到用户 _**hpc4you**_, 搜索 _**hpc4you toolkit**_ , 自行选择观看.
4. 视频仓库, 阿里云盘「hpc4you_toolkit-操作实况录像」[https://www.aliyundrive.com/s/GrcXoWrccTP](https://www.aliyundrive.com/s/GrcXoWrccTP)
5. 无论哪一个系统, 都是复制、粘贴屏幕提示的绿色内容. 分别做那么多实况录像, 就是用实际案例演示, 的确是“零配置、傻瓜式“操作.

## 文字手册

- 集群
  1. Web 可视化, hpc4you_toolkit v3.1-Web, 🔗[请点击这里查看](http://tophpc.top:1080/doc/hpc4you-toolkit-manual/v3.1/).
  2. 字符/指令界面, hpc4you_toolkit v2, 🔗[请点击这里查看](http://tophpc.top:1080/doc/hpc4you-toolkit-manual/v2/).
- 服务器/工作站/单机
  1. Web 可视化, hpc4you_toolkit-**solo** v3.1-Web, 🔗[请点击这里查看](http://tophpc.top:1080/doc/hpc4you-toolkit-manual/solo-Web/).
  2. 字符/指令界面, hpc4you_toolkit-**solo** v2, 🔗[请点击这里查看](http://tophpc.top:1080/doc/hpc4you-toolkit-manual/solo/).

## 常见问题/FAQ

操作层面, 仅需`Copy+Paste+Enter`.
相关的准备工作, 以及与此相关的 Linux 故障处理, 请点击查看[常见问题/FAQ](https://gitee.com/hpc4you/hpc/blob/master/FAQ.md).

## 使用须知/TOS

为提高沟通效率, 避免不必要的纠纷, 请点击阅[用户须知](https://gitee.com/hpc4you/hpc/blob/master/TOS.md).