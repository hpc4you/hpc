# 可用版本

## v2.0 单机

_**hpc4you toolkit solo**_, 单机/工作站/服务器傻瓜式自动部署 slurm 调度器.

上传工具包, 输入`unzip hpc4you*zip; source code`即可自动完成安装.

## v3.0 单机

_**hpc4you toolkit solo Web**_,
单机/工作站/服务器傻瓜式自动部署 slurm 调度器和 Web 操作界面.

上传工具包, 输入指令`unzip hpc4you*zip; source code`即可自动完成安装.

操作演示, [BV1Gg411R7tt](https://www.bilibili.com/video/BV1Gg411R7tt)🔗.

## v2.0 集群

适用平台

1. RHEL7, RHEL8, RHEL9 及其兼容系统, 比如 CentOS 7.x, 8.x, 8 Stream, AnolisOS 8.x, Rocky Linux, AlmaLinux 9 等;
2. Ubuntu 20.04 和 Ubuntu 22.04;
3. 华为 OpenEuler 22.03 LTS.

## v3.0 集群

最大特点, HPC via Web, 亦即提供基于浏览器的图形操作界面, 包括但不限于:

1. 添加/删除用户和用户组, 在 Web 界面通过点击鼠标完成
2. 资源分配/管理, 在 Web 界面通过点击鼠标完成, 支持课题组长/PI 自主管理用户组
3. 计算任务提交/查看, 点击鼠标完成, 输入文件上传/修改, 输出查看/下载等等, 均在浏览器中点击鼠标进行.
4. 支持在浏览器中, 直接开启 Linux 桌面, 可以运行任何需要桌面环境的程序, 比如 Matlab, Mathematica 等.

适用平台

1. RHEL7、RHEL8、RHEL9 及其兼容系统.
2. Ubuntu 20.04 和 Ubuntu 22.04.

图文并貌操作说明以及操作演示视频, 请[点击查看阿里云盘](https://www.aliyundrive.com/s/wTtser9MsjR).

## CentOS8

CentOS8.x, 是 RHEL8 的兼容系统.
使用 CentOS8.x 的朋友请留意, 请配置好系统的 repo, 确保 dnf 工作正常.
更多细节, 请查阅(https://gitee.com/hpc4you/linux/tree/master/repos/centOS8).